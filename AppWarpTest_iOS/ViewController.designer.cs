// WARNING
//
// This file has been generated automatically by Xamarin Studio from the outlets and
// actions declared in your storyboard file.
// Manual changes to this file will not be maintained.
//
using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;

namespace AppWarpTest_iOS
{
	[Register ("ViewController")]
	partial class ViewController
	{
		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton clearBtn { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton connectBtn { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton disconnectBtn { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UITextField inputTextView { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UIButton JoinBtn { get; set; }

		[Outlet]
		[GeneratedCode ("iOS Designer", "1.0")]
		UICollectionView messageListView { get; set; }

		[Action ("clearBtn_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void clearBtn_TouchUpInside (UIButton sender);

		[Action ("connectBtn_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void connectBtn_TouchUpInside (UIButton sender);

		[Action ("disconnectBtn_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void disconnectBtn_TouchUpInside (UIButton sender);

		[Action ("JoinBtn_TouchUpInside:")]
		[GeneratedCode ("iOS Designer", "1.0")]
		partial void JoinBtn_TouchUpInside (UIButton sender);

		void ReleaseDesignerOutlets ()
		{
			if (clearBtn != null) {
				clearBtn.Dispose ();
				clearBtn = null;
			}
			if (connectBtn != null) {
				connectBtn.Dispose ();
				connectBtn = null;
			}
			if (disconnectBtn != null) {
				disconnectBtn.Dispose ();
				disconnectBtn = null;
			}
			if (inputTextView != null) {
				inputTextView.Dispose ();
				inputTextView = null;
			}
			if (JoinBtn != null) {
				JoinBtn.Dispose ();
				JoinBtn = null;
			}
			if (messageListView != null) {
				messageListView.Dispose ();
				messageListView = null;
			}
		}
	}
}
