﻿using System;

using UIKit;
using System.Collections.Generic;
using CoreGraphics;
using Foundation;
using AppWarpTest_Portable;
using System.Threading.Tasks;

namespace AppWarpTest_iOS
{
	public partial class ViewController : UIViewController, IUICollectionViewDelegate, IMessagePrinter, IRootPage
	{
		private List<string> msgs;
		private ProgressDialog progDialog;

		public ViewController (IntPtr handle) : base (handle)
		{
			AppServices.InitAppWarp(this);
			AppServices.rootPage = this;
		}

		public override void ViewDidLoad ()
		{
			base.ViewDidLoad ();
			// Perform any additional setup after loading the view, typically from a nib.
			AppDelegate.currPage = this;

			msgs = new List<string>();
			this.messageListView.DataSource = new MsgCollViewDataSrc(msgs);
			this.messageListView.Delegate = this;
			this.messageListView.AllowsMultipleSelection = true;
			this.messageListView.AddGestureRecognizer(new UITapGestureRecognizer(gesture =>
				{
					var tapPoint = gesture.LocationInView(this.messageListView);
					var indexPath = this.messageListView.IndexPathForItemAtPoint(tapPoint);
					if (indexPath != null)
					{
						// Item tapped handler
					}
				}));

			var lastInput = NSUserDefaults.StandardUserDefaults.StringForKey("input");
			if (string.IsNullOrWhiteSpace(lastInput) == false)
				this.inputTextView.Text = lastInput;

		}

		public override void ViewDidAppear (bool animated)
		{
			base.ViewDidAppear (animated);
			// You must set the size of the items here as the correct bounds are not available before this
			this.messageListView.CollectionViewLayout = new UICollectionViewFlowLayout() { ItemSize = new CGSize(this.View.Frame.Width/2-(17*2), this.messageListView.Frame.Height),
				ScrollDirection = UICollectionViewScrollDirection.Horizontal};
		}

		public override void DidReceiveMemoryWarning ()
		{
			base.DidReceiveMemoryWarning ();
			// Release any cached data, images, etc that aren't in use.
		}

		[Foundation.Export ("collectionView:didHighlightItemAtIndexPath:")]
		public void ItemHighlighted (UIKit.UICollectionView collectionView, Foundation.NSIndexPath indexPath)
		{
			var cell = collectionView.CellForItem(indexPath) as template_MsgCell;
			cell.contentView.BackgroundColor = UIColor.FromRGB(0, 200, 0);
		}

		[Foundation.Export ("collectionView:didUnhighlightItemAtIndexPath:")]
		public void ItemUnhighlighted (UIKit.UICollectionView collectionView, Foundation.NSIndexPath indexPath)
		{
			var cell = collectionView.CellForItem(indexPath) as template_MsgCell;
			cell.contentView.BackgroundColor = UIColor.FromRGB(0, 0, 200);
		}

		partial void connectBtn_TouchUpInside (UIButton sender)
		{
			var textView = this.inputTextView;
			var username = textView.Text;
			if (string.IsNullOrWhiteSpace(username) == false)
			{
				AppServices.Connect(username);
				textView.Text = "";
			}
			else
				AppServices.Connect("player1");
		}

		partial void JoinBtn_TouchUpInside (UIButton sender)
		{
			AppServices.CreateAndJoinRoom(this.inputTextView.Text);
		}

		partial void disconnectBtn_TouchUpInside (UIButton sender)
		{
			AppServices.Disconnect();
		}

		partial void clearBtn_TouchUpInside (UIButton sender)
		{
			this.msgs.Clear();
			this.messageListView.ReloadData();
		}
			
		#region IMessagePrinter implementation

		public void PrintMessage (string message)
		{
			this.InvokeOnMainThread(() => 
				{
					this.msgs.Add(message);
					var indexPath = NSIndexPath.FromRowSection(this.msgs.Count-1, 0);
					this.messageListView.InsertItems(new NSIndexPath[] { indexPath });
					this.messageListView.ScrollToItem(indexPath, UICollectionViewScrollPosition.Right, true);
				});
		}

		#endregion

		#region IRootPage implementation

		public void ShowProgressDialog (string text)
		{
			this.InvokeOnMainThread(() =>
				{
					// Determine the correct size to start the overlay (depending on device orientation)
					var bounds = UIScreen.MainScreen.Bounds; // portrait bounds
					if (UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeLeft || UIApplication.SharedApplication.StatusBarOrientation == UIInterfaceOrientation.LandscapeRight) {
						bounds.Size = new CGSize(bounds.Size.Height, bounds.Size.Width);
					}
					// show the loading overlay on the UI thread using the correct orientation sizing
					this.HideProgressDialog();
					this.progDialog = new ProgressDialog(bounds, text, "Please Wait");
					this.View.Add(this.progDialog);
				});		
		}

		public void HideProgressDialog ()
		{
			this.InvokeOnMainThread(() =>
				{
					if (this.progDialog != null)
						this.progDialog.Hide();
				});	
		}

		#endregion

		public virtual void AppPaused()
		{
			
		}

		public virtual void AppStopped()
		{
			// Save user input
			NSUserDefaults.StandardUserDefaults.SetString(this.inputTextView.Text, "input");
		}

		public virtual void AppResumed()
		{
			
		}

		public virtual void AppStarted()
		{
			
		}
	}
}

