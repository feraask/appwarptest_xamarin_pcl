using Foundation;
using System;
using System.CodeDom.Compiler;
using UIKit;
using CoreGraphics;

namespace AppWarpTest_iOS
{
	partial class template_MsgCell : UICollectionViewCell
	{
		public UIView contentView { get { return this.msgTxtView; } }
		public template_MsgCell (IntPtr handle) : base (handle)
		{
			this.SelectedBackgroundView = new UIView { BackgroundColor = UIColor.FromRGB(0, 200, 0) };
		}

		public void UpdateUI(string msg)
		{
			this.contentView.Layer.CornerRadius = 10.0f;
			this.msgTxtView.Text = msg;
		}

	}
}
