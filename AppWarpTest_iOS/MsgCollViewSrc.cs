﻿using System;
using UIKit;
using System.Collections.Generic;

namespace AppWarpTest_iOS
{
	public class MsgCollViewDataSrc : UICollectionViewDataSource
	{
		const string cellID = "messageCell";
		List<string> items;
	
		public MsgCollViewDataSrc (List<string> messages)
		{
			this.items = messages;
		}

		public override nint NumberOfSections (UICollectionView collectionView)
		{
			return 1;
		}

		public override nint GetItemsCount (UICollectionView collectionView, nint section)
		{
			return items.Count;
		}

		public override UICollectionViewCell GetCell (UICollectionView collectionView, Foundation.NSIndexPath indexPath)
		{
			var msgCell = collectionView.DequeueReusableCell(cellID, indexPath) as template_MsgCell;
			msgCell.Layer.CornerRadius = 10.0f;
			msgCell.UpdateUI(this.items[indexPath.Row]);
			return msgCell;
		}
	}
}

