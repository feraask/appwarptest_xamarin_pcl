﻿using System;

namespace AppWarpTest_Portable
{
	public enum Colors {red, green, blue, white}

	public class MsgListItem
	{
		public string text {
			get;
			set;
		}

		public Colors color {
			get;
			set;
		}

		public MsgListItem ()
		{
			this.color = Colors.white;
		}
	}
}

