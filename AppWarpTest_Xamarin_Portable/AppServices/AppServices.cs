﻿using System;
using com.shephertz.app42.gaming.multiplayer.client;
using System.Threading.Tasks;
using com.shephertz.app42.gaming.multiplayer.client.command;
using System.Collections.Generic;
using Newtonsoft.Json;

namespace AppWarpTest_Portable
{
	public static class AppServices
	{
		public const string apiKey = "REPLACE_WITH_API_KEY";
		public const string secretKey = "REPLACE_WITH_SECRET_KEY";
		public static IMessagePrinter page;
		public static IRootPage rootPage;

		public static WarpClient GetWarpClient() {return WarpClient.GetInstance();}

		public static void InitAppWarp(IMessagePrinter page)
		{
			AppServices.page = page;
			WarpClient.initialize(AppServices.apiKey, AppServices.secretKey);
			WarpClient.setRecoveryAllowance(30);
			WarpClient.GetInstance().AddConnectionRequestListener(new ConnectionListener {_page = page});
			WarpClient.GetInstance().AddNotificationListener(new NotificationListener {_page = page});
			WarpClient.GetInstance().AddRoomRequestListener(new RoomListener {_page = page});
			WarpClient.GetInstance().AddZoneRequestListener(new ZoneListener {_page = page});
		}

		public static string Serialize(Object obj)
		{
			return JsonConvert.SerializeObject(obj);
		}

		public static T Deserialize<T>(string jsonString)
		{
			return JsonConvert.DeserializeObject<T>(jsonString);
		}

		public static void Connect(string username)
		{
			AppServices.page.PrintMessage(String.Format("Connecting as {0}", username));
			WarpClient.GetInstance().Connect(username);
		}

		public static void Disconnect()
		{
			AppServices.page.PrintMessage("Disconnecting...");
			WarpClient.GetInstance().Disconnect();
		}

		public static void CreateAndJoinRoom(string roomID = null)
		{
			// Create a new room
			if (string.IsNullOrWhiteSpace(roomID))
			{
				AppServices.page.PrintMessage("Creating room...");
				WarpClient.GetInstance().CreateRoom("player's room", "player", 10, null, 10);
			}
			// Join the given room
			else
			{
				AppServices.page.PrintMessage(String.Format("Joining room {0}", roomID));
				WarpClient.GetInstance().JoinRoom(roomID);
				WarpClient.GetInstance().SubscribeRoom(roomID);
			}
		}
	}

	public class ConnectionListener : com.shephertz.app42.gaming.multiplayer.client.listener.ConnectionRequestListener
	{
		public IMessagePrinter _page;
		public ConnectionListener()
		{ }

		public async void onConnectDone(com.shephertz.app42.gaming.multiplayer.client.events.ConnectEvent eventObj)
		{
			byte result = eventObj.getResult();
			string msg = null;

			switch (result)
			{
			case WarpResponseResultCode.SUCCESS:
				msg = "Connected Successfully. Session ID: " + WarpClient.GetInstance().GetSessionId().ToString();
				break;
			case WarpResponseResultCode.AUTH_ERROR:
				msg = "Connect AUTH_ERROR.";
				//byte reasonCode = eventObj.getReasonCode();
				//switch (reasonCode)
				//{
				//	case WarpReasonCode.WAITING_FOR_PAUSED_USER:
				//		msg = "Connect AUTH_ERROR Reason Code: Waiting for Paused User (" + reasonCode + ")";
				//		break;
				//	default:
				//		msg = "Connect AUTH_ERROR Reason Code: " + reasonCode;
				//		break;
				//}
				break;
			case WarpResponseResultCode.SUCCESS_RECOVERED:
				msg = "Connection Recovered!";
				break;
			case WarpResponseResultCode.CONNECTION_ERROR_RECOVERABLE:
				_page.PrintMessage("Connection error recoverable occured. waiting 15 seconds to recover...");
				await Task.Delay(15000);
				_page.PrintMessage("recovering...");
				WarpClient.GetInstance().RecoverConnection();
				return;
			default:
				msg = "unknown connection error occured";
				break;
			}

			_page.PrintMessage(msg);
		}

		public void onDisconnectDone(com.shephertz.app42.gaming.multiplayer.client.events.ConnectEvent eventObj)
		{
			byte result = eventObj.getResult();
			string msg = null;

			switch (result)
			{
			case WarpResponseResultCode.SUCCESS:
				msg = "Disconnect Successfully!";
				break;
			case WarpResponseResultCode.AUTH_ERROR:
				msg = "DISCONNECT AUTH_ERROR";
				//byte reasonCode = eventObj.getReasonCode();
				//switch (reasonCode)
				//{
				//	case WarpReasonCode.WAITING_FOR_PAUSED_USER:
				//		msg = "Disconnect AUTH_ERROR Reason Code: Waiting for Paused User (" + reasonCode + ")";
				//		break;
				//	default:
				//		msg = "Disconnect AUTH_ERROR Reason Code: " + reasonCode;
				//		break;
				//}
				break;
			default:
				msg = "unknown disconnect error occured";
				break;
			}

			_page.PrintMessage(msg);
		}


		public void onInitUDPDone(byte resultCode)
		{

		}
	}

	public class NotificationListener : com.shephertz.app42.gaming.multiplayer.client.listener.NotifyListener
	{
		public IMessagePrinter _page;
		public NotificationListener()
		{ }

		public void onChatReceived(com.shephertz.app42.gaming.multiplayer.client.events.ChatEvent eventObj)
		{
		}

		public void onGameStarted(string sender, string roomId, string nextTurn)
		{
		}

		public void onGameStopped(string sender, string roomId)
		{
		}

		public void onMoveCompleted(com.shephertz.app42.gaming.multiplayer.client.events.MoveEvent moveEvent)
		{
		}

		public void onPrivateChatReceived(string sender, string message)
		{
		}

		public void onPrivateUpdateReceived(string sender, byte[] update, bool fromUdp)
		{
		}

		public void onRoomCreated(com.shephertz.app42.gaming.multiplayer.client.events.RoomData eventObj)
		{
		}

		public void onRoomDestroyed(com.shephertz.app42.gaming.multiplayer.client.events.RoomData eventObj)
		{
		}

		public void onUpdatePeersReceived(com.shephertz.app42.gaming.multiplayer.client.events.UpdateEvent eventObj)
		{
		}

		public void onUserChangeRoomProperty(com.shephertz.app42.gaming.multiplayer.client.events.RoomData roomData, string sender, Dictionary<string, object> properties, Dictionary<string, string> lockedPropertiesTable)
		{
		}

		public void onUserJoinedLobby(com.shephertz.app42.gaming.multiplayer.client.events.LobbyData eventObj, string username)
		{
		}

		public void onUserLeftLobby(com.shephertz.app42.gaming.multiplayer.client.events.LobbyData eventObj, string username)
		{
		}

		public void onUserJoinedRoom(com.shephertz.app42.gaming.multiplayer.client.events.RoomData eventObj, string username)
		{
			_page.PrintMessage(username + " joined room");
		}

		public void onUserLeftRoom(com.shephertz.app42.gaming.multiplayer.client.events.RoomData eventObj, string username)
		{
			_page.PrintMessage(username + " left room");
		}

		public void onUserPaused(string locid, bool isLobby, string username)
		{
			_page.PrintMessage(username + " paused");
		}

		public void onUserResumed(string locid, bool isLobby, string username)
		{
			_page.PrintMessage(username + " resumed with connection state: " + WarpClient.GetInstance().GetConnectionState());
		}

		public void onNextTurnRequest(string lastTurn)
		{
		}
	}

	public class RoomListener : com.shephertz.app42.gaming.multiplayer.client.listener.RoomRequestListener
	{
		public IMessagePrinter _page;

		public void onGetLiveRoomInfoDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent eventObj)
		{
		}

		public void onJoinRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
			if (eventObj.getResult() == WarpResponseResultCode.SUCCESS)
			{
				string roomID = eventObj.getData().getId();
				_page.PrintMessage("Joined room: " + roomID);
			}
			else
			{
				_page.PrintMessage("Join room error code: " + eventObj.getResult());
			}
		}

		public void onLeaveRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
		}

		public void onLockPropertiesDone(byte result)
		{
		}

		public void onSetCustomRoomDataDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent eventObj)
		{
		}

		public void onSubscribeRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
			if (eventObj.getResult() == WarpResponseResultCode.SUCCESS)
			{
				string roomID = eventObj.getData().getId();
				_page.PrintMessage("subscribed to room: " + roomID);
			}
			else
			{
				_page.PrintMessage("subscribe room error code: " + eventObj.getResult());
			}
		}

		public void onUnSubscribeRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
		}

		public void onUnlockPropertiesDone(byte result)
		{
		}

		public void onUpdatePropertyDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveRoomInfoEvent lifeLiveRoomInfoEvent)
		{
		}
	}

	public class ZoneListener : com.shephertz.app42.gaming.multiplayer.client.listener.ZoneRequestListener
	{
		public IMessagePrinter _page;

		public void onCreateRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
			string roomID = eventObj.getData().getId();
			_page.PrintMessage("created room id: " + roomID);
			_page.PrintMessage("joining & subscribing to room...");
			WarpClient.GetInstance().JoinRoom(roomID);
			WarpClient.GetInstance().SubscribeRoom(roomID);
		}

		public void onDeleteRoomDone(com.shephertz.app42.gaming.multiplayer.client.events.RoomEvent eventObj)
		{
		}

		public void onGetAllRoomsDone(com.shephertz.app42.gaming.multiplayer.client.events.AllRoomsEvent eventObj)
		{
		}

		public void onGetLiveUserInfoDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveUserInfoEvent eventObj)
		{
		}

		public void onGetMatchedRoomsDone(com.shephertz.app42.gaming.multiplayer.client.events.MatchedRoomsEvent matchedRoomsEvent)
		{
		}

		public void onGetOnlineUsersDone(com.shephertz.app42.gaming.multiplayer.client.events.AllUsersEvent eventObj)
		{
		}

		public void onSetCustomUserDataDone(com.shephertz.app42.gaming.multiplayer.client.events.LiveUserInfoEvent eventObj)
		{
		}
	}
}

