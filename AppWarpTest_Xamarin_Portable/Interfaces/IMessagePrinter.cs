﻿using System;

namespace AppWarpTest_Portable
{
	public interface IMessagePrinter
	{
		void PrintMessage(string message);
	}
}

