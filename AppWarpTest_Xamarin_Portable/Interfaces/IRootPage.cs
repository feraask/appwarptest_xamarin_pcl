﻿using System;

namespace AppWarpTest_Portable
{
	public interface IRootPage
	{
		void ShowProgressDialog(string text);
		void HideProgressDialog();
	}
}

