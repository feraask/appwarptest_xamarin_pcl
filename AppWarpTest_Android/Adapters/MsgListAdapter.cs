﻿using System;
using Android.Widget;
using Android.App;
using System.Collections.Generic;
using AppWarpTest_Portable;
using Android.Support.V7.Widget;
using Android.Views;

namespace AppWarpTest_Android 
{
	public class MsgListAdapter : RecyclerView.Adapter
	{
		private List<MsgListItem> _items;
		private Dictionary<int, bool> selectedItems = new Dictionary<int, bool>();
		public event EventHandler<int> ItemClick;

		public MsgListAdapter (List<MsgListItem> items)
		{
			this._items = items;
		}

		public override RecyclerView.ViewHolder OnCreateViewHolder (Android.Views.ViewGroup parent, int viewType)
		{
			var itemView = LayoutInflater.From(parent.Context).Inflate(Resource.Layout.MsgListItem, parent, false);
			var vh = new MsgListViewHolder(itemView, this.OnClick);
			return vh;
		}

		public override void OnBindViewHolder (RecyclerView.ViewHolder holder, int position)
		{
			var vh = holder as MsgListViewHolder;
			vh.msgText.Text = this._items[position].text;
			if (this.selectedItems.ContainsKey(position))
			{
				//vh.itemCard.SetCardBackgroundColor(Android.Graphics.Color.DarkBlue);
				vh.checkImage.Visibility = ViewStates.Visible;
				vh.bkgndCard.Visibility = ViewStates.Visible;
			}
			else
			{
				//vh.itemCard.SetCardBackgroundColor(Android.Graphics.Color.DarkOliveGreen);
				vh.checkImage.Visibility = ViewStates.Invisible;
				vh.bkgndCard.Visibility = ViewStates.Invisible;
			}
		}

		public override int ItemCount {	get { return this._items.Count; } }

		void OnClick(int position)
		{
			// Setup for an external handler
			if (ItemClick != null)
				ItemClick(this, position);

			if (this.selectedItems.ContainsKey(position))
			{
				this.selectedItems.Remove(position);
			}
			else
			{
				this.selectedItems.Add(position, true);
			}
			this.NotifyItemChanged(position);
		}

		public void Add(MsgListItem item)
		{
			this._items.Add(item);
			this.NotifyItemInserted(this._items.Count-1);
		}

		public void Clear()
		{
			this._items.Clear();
			this.selectedItems.Clear();
			this.NotifyDataSetChanged();
		}
//
//		public void Remove(int index)
//		{
//			this._items.RemoveAt(index);
//			this.NotifyDataSetChanged();
//		}
//
//		public void Remove(MsgListItem item)
//		{
//			int index = 0;
//			foreach (MsgListItem msg in this._items)
//			{
//				if (msg.text.Equals(item.text))
//					break;
//				index++;
//			}
//			this.Remove(index);
//		}

//		public override long GetItemId (int position)
//		{
//			return position;
//		}
//
//		public override MsgListItem this[int index] 
//		{
//			get { return _items[index];	}
//		}
//
//		public override int Count 
//		{
//			get { return _items.Count;	}
//		}
//
//		public override Android.Views.View GetView (int position, Android.Views.View convertView, Android.Views.ViewGroup parent)
//		{
//			var item = _items[position];
//			var view = convertView;
//			if (view == null)
//				view = this._context.LayoutInflater.Inflate(Resource.Layout.MsgListItem, null);
//			var msgTextView = view.FindViewById<TextView>(Resource.Id.msgTextView);
//			msgTextView.Text = item.text;
//			this.SetItemColor(item, msgTextView);
//			return view;
//		}

		private void SetItemColor(MsgListItem item, TextView view)
		{
			switch (item.color)
			{
				case Colors.red:
					view.SetTextColor(Android.Graphics.Color.Red);
					break;
				case Colors.green:
					view.SetTextColor(Android.Graphics.Color.Green);
					break;
				case Colors.blue:
					view.SetTextColor(Android.Graphics.Color.Blue);
					break;
				default:
					view.SetTextColor(Android.Graphics.Color.White);
					break;
			}
		}
	}
}

