﻿using AppWarpTest_Portable;
using System;
using System.Collections.Generic;

using Android.Support.V4.App;
using Android.Content;
using Android.OS;
using Android.Views;
using Android.Widget;
using System.Threading.Tasks;
using Android.Support.V7.Widget;

namespace AppWarpTest_Android
{
	public class MainFragment : Android.Support.V4.App.Fragment, IMessagePrinter
	{
		private List<MsgListItem> msgs;
		RecyclerView mRecyclerView;
		MsgListAdapter mAdapter;
		RecyclerView.LayoutManager mLayoutManager;

		public override void OnCreate (Bundle savedInstanceState)
		{
			base.OnCreate (savedInstanceState);
		
			// Create your fragment here
			AppServices.InitAppWarp(this);
		}

		public override View OnCreateView (LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
		{
			return inflater.Inflate(Resource.Layout.MainFragment, container, false);
		}

		public override void OnActivityCreated (Bundle savedInstanceState)
		{
			base.OnActivityCreated(savedInstanceState);

			if (this.msgs == null)
			{
				var sharedPref = this.Activity.GetPreferences(FileCreationMode.Private);
				string listJson = sharedPref.GetString("msgList", null);
				if (listJson != null)
				{
					this.msgs = new List<MsgListItem>(AppServices.Deserialize<MsgListItem[]>(listJson));
				}
				else
					this.msgs = new List<MsgListItem>();
			}
			this.mAdapter = new MsgListAdapter(this.msgs);
			this.mAdapter.ItemClick += OnItemClick;
			this.mRecyclerView = Activity.FindViewById<RecyclerView>(Resource.Id.msgListView);
			this.mRecyclerView.SetAdapter(this.mAdapter);
			mLayoutManager = new GridLayoutManager(this.Activity, 1, GridLayoutManager.Horizontal, false);
			this.mRecyclerView.SetLayoutManager(mLayoutManager);

			Activity.FindViewById<Button>(Resource.Id.clearBtn).Click += clearBtn_Click;
			Activity.FindViewById<Button>(Resource.Id.connectBtn).Click += connectBtn_Click;
			Activity.FindViewById<Button>(Resource.Id.disconnectBtn).Click += disconnectBtn_Click;
			Activity.FindViewById<Button>(Resource.Id.joinBtn).Click += joinBtn_Click;
		}

		public override async void OnPause ()
		{
			base.OnPause();
			await Task.Run(() =>
				{
					var sharedPref = this.Activity.GetPreferences(FileCreationMode.Private);
					var editor = sharedPref.Edit();
					string serialized = AppServices.Serialize(this.msgs.ToArray());
					editor.PutString("msgList", serialized);
					editor.Commit();
				});

		}

		void OnItemClick (object sender, int position)
		{
		}

		void joinBtn_Click (object sender, EventArgs e)
		{
			AppServices.CreateAndJoinRoom(Activity.FindViewById<TextView>(Resource.Id.inputTextView).Text);
		}

		void disconnectBtn_Click (object sender, EventArgs e)
		{
			AppServices.Disconnect();
		}

		void connectBtn_Click (object sender, EventArgs e)
		{
			var textView = Activity.FindViewById<TextView>(Resource.Id.inputTextView);
			var username = textView.Text;
			if (string.IsNullOrWhiteSpace(username) == false)
			{
				AppServices.Connect(username);
				textView.Text = "";
			}
			else
				AppServices.Connect("player1");
		}

		void clearBtn_Click (object sender, EventArgs e)
		{
			this.GetMsgsAdapter().Clear();
		}

		private MsgListAdapter GetMsgsAdapter()
		{
			return Activity.FindViewById<RecyclerView>(Resource.Id.msgListView).GetAdapter() as MsgListAdapter;
		}

		#region IMessagePrinter implementation

		public void PrintMessage (string message)
		{
			Activity.RunOnUiThread(async () =>
				{
					var adapter = this.GetMsgsAdapter();
					adapter.Add(new MsgListItem {text = message});
					await System.Threading.Tasks.Task.Delay(500);
					Activity.FindViewById<RecyclerView>(Resource.Id.msgListView).SmoothScrollToPosition(adapter.ItemCount-1);
				});
		}

		#endregion
	}
}

