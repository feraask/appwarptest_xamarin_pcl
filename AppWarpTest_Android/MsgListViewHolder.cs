﻿using System;
using Android.Support.V7.Widget;
using Android.Views;
using Android.Widget;

namespace AppWarpTest_Android
{
	public class MsgListViewHolder : RecyclerView.ViewHolder
	{
		public TextView msgText { get; private set; }
		public View itemCard { get; private set; }
		public View bkgndCard { get; private set; }
		public ImageView checkImage { get; private set; }

		public MsgListViewHolder (View itemView, Action<int> listener) : base(itemView)
		{
			this.msgText = itemView.FindViewById<TextView>(Resource.Id.msgTextView);
			this.itemCard = itemView.FindViewById<View>(Resource.Id.msgCardView);
			this.checkImage = itemView.FindViewById<ImageView>(Resource.Id.msgItemCheckImage);
			this.bkgndCard = itemView.FindViewById<View>(Resource.Id.msgBkgndCardView);
			this.bkgndCard.SetBackgroundColor(Android.Graphics.Color.Cyan);
			itemView.Click += (sender, e) => listener(this.Position);
		}
	}
}

