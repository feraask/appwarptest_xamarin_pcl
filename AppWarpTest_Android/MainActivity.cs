﻿using Android.App;
using Android.OS;
using AppWarpTest_Portable;
using Android.Support.V4.App;

namespace AppWarpTest_Android
{
	[Activity (Label = "AppWarpTest Android Portable", MainLauncher = true, Icon = "@drawable/icon", ScreenOrientation = Android.Content.PM.ScreenOrientation.Portrait)]
	public class MainActivity : FragmentActivity, IRootPage
	{
		protected override void OnCreate (Bundle bundle)
		{
			base.OnCreate (bundle);
			// Set our view from the "main" layout resource
			SetContentView (Resource.Layout.Main);
			var fragTx = this.SupportFragmentManager.BeginTransaction();
			fragTx.Replace(Resource.Id.rootLayout, new MainFragment());
			fragTx.Commit();
			AppServices.rootPage = this;
		}


		#region IRootPage implementation

		public void HideProgressDialog ()
		{
			var curr = this.SupportFragmentManager.FindFragmentByTag("progressBar");
			if (curr != null)
			{
				var fragTx = this.SupportFragmentManager.BeginTransaction();
				fragTx.Remove(curr);
				fragTx.Commit();
			}
		}
		public void ShowProgressDialog (string text)
		{
		}
		#endregion
	}
}


